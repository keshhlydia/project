

<?php if ($expression == true): ?>
  This will show if the expression is true.
<?php else: ?>
  Otherwise this will show.
<?php endif; ?> 

<?php echo '1'; ?>
 <?php echo '2'; ?>_
<?php echo '3'; ?>
_<?php echo '4'; ?>_<?php echo '5'; ?>
 

<p>This is ignore by the php parser and displayed by the browser </p>

  <?php echo "While this is going to be parsed"; ?>
  <br>


  <?php $a = 10; if($a<100): ?>
  This conditional block is executed
   <?php else: ?>
      otherwise this will be executed
       <?php endif; ?>

       <br>

       <html>
<?php
function show($a) {
     ?>
    <a href="https://www.<?php echo $a ?>.com">
     Link
     </a>
     <?php
}
?>
<body>
     <?php show("google") ?>
</body>
 </html>

 <br>

 <html><body><a href="https://www.google.com">Link</a></body></html>
 <br>

 <?php echo "Some text"; ?>
No newline
<?= "But newline now" ?> 
<br>

<?php
    echo 'This is a test';
?>

<?php echo 'This is a test' ?>

<?php echo 'We omitted the last closing tag'; 

echo "a"; echo "b"; echo "c";
#The output will be "abc" with no errors
?> 

<br>
<?php echo "first statement";
       echo "second statement"
?>
<br>
<?php
$a_bool = TRUE;   // a boolean
$a_str  = "foo";  // a string
$a_str2 = 'foo';  // a string
$an_int = 12;     // an integer

echo gettype($a_bool); // prints out:  boolean
echo gettype($a_str);  // prints out:  string

// If this is an integer, increment it by four
if (is_int($an_int)) {
    $an_int += 4;
}

// If $a_bool is a string, print it out
// (does not print out anything)
if (is_string($a_bool)) {
    echo "String: $a_bool";
}
?> 


<br>
<?php
$foo = True; // assign the value TRUE to $foo
?> 
<br>
<?php
// == is an operator which tests
// equality and returns a boolean
if ($action == "show_version") {
    echo "The version is 1.23";
}

// this is not necessary...
if ($show_separators == TRUE) {
    echo "<hr>\n";
}

// ...because this can be used with exactly the same meaning:
if ($show_separators) {
    echo "<hr>\n";
}
?> 

<br>
<?php
var_dump((bool) "");        // bool(false)
var_dump((bool) "0");       // bool(false)
var_dump((bool) 1);         // bool(true)
var_dump((bool) -2);        // bool(true)
var_dump((bool) "foo");     // bool(true)
var_dump((bool) 2.3e5);     // bool(true)
var_dump((bool) array(12)); // bool(true)
var_dump((bool) array());   // bool(false)
var_dump((bool) "false");   // bool(true)
?> 

<br><?php
$str = <<<EOD
Example of string
spanning multiple lines
using heredoc syntax.
EOD;

/* More complex example, with variables. */
class foo
{
    var $foo;
    var $bar;

    function __construct()
    {
        $this->foo = 'Foo';
        $this->bar = array('Bar1', 'Bar2', 'Bar3');
    }
}

$foo = new foo();
$name = 'MyName';

echo <<<EOT
My name is "$name". I am printing some $foo->foo.
Now, I am printing some {$foo->bar[1]}.
This should print a capital 'A': \x41
EOT;
?> 
